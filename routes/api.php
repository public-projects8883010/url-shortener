<?php

use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'api'], function () {
    Route::post('/shorten', [ApiController::class, 'store']);
});
