<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About This Project

This project is a URL shortener application built with Laravel and Vue.js, designed to be run in a Docker environment. It allows users to submit a URL and receive a shortened version, which can then be used to redirect to the original URL.

## Requirements

- Docker
- Docker Compose

## Installation

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/your-username/url-shortener.git
   cd url-shortener

2. **Copy the .env.example file to .env and set your environment variables:**

   ```bash
   cp .env.example .env

3. **Build and start the Docker containers:**

   ```bash
   docker-compose up -d --build

4. **Install PHP dependencies:**

    ```bash
   docker-compose up -d --build

5. **Generate the application key:**

    ```bash
    docker-compose run --rm app php artisan key:generate

6. **Run database migrations:**

    ```bash
    docker-compose run --rm app php artisan migrate

7. **Install Node.js dependencies and build the frontend:**

    ```bash
    docker-compose run --rm node npm install
    docker-compose run --rm node npm run dev

8. **Create files using artisan command:**

    ```bash
        docker compose run --rm --user root app php artisan make:request StoreUrlReques
        then add user access to edit with editor
        sudo chown -R $USER:$USER app/Http/Requests/

9. **To run laravel test please use the following:**

    ```bash
    docker compose run --rm app php artisan test

10. **Easy access to phpmyadmin**

    ```bash
    http://localhost:8080

## Usage

- The application will be accessible at <http://localhost:8000>.
- To shorten a URL, submit it through the form on the main page.
- Use the generated short URL to be redirected to the original URL.

## Troubleshooting

**Permission Issues**
If you encounter a Permission denied error for the storage or cache directories, you can fix it by setting the correct permissions using the root user:

```bash
  # Set the correct permissions for storage and bootstrap/cache directories
  docker-compose run --rm --user root app chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
```

For example, you might see an error like this:

```ruby
   file_put_contents(/var/www/storage/framework/views/187f828346b000af3c7029fb05171792.php): Failed to open stream: Permission denied
```

This can be fixed by running the following command:

```bash
   docker-compose run --rm --user root app chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache
```

Rebuilding Containers:

```bash
   docker-compose up -d --build
```
