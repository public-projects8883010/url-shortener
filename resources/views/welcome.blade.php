<!DOCTYPE html>
<html>

<head>
    <title>URL Shortener</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @vite('resources/css/app.css')
</head>

<body>
    <div id="app">
        <url-form></url-form>
    </div>
    @vite('resources/js/app.js')
</body>

</html>
