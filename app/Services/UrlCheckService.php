<?php

namespace App\Services;

use GuzzleHttp\Client;

class UrlCheckService
{
    protected $client;
    protected $apiKey;

    public function __construct()
    {
        $this->client = new Client();
        $this->apiKey = config('google.safe_browsing_api_key');
    }

    public function isUnsafeUrl($url): bool
    {
        try {
            $response = $this->client->post('https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' . $this->apiKey, [
                'json' => [
                    'client' => [
                        'clientId' => 'url-shortener',
                        'clientVersion' => '1.0.0'
                    ],
                    'threatInfo' => [
                        'threatTypes' => ['MALWARE', 'SOCIAL_ENGINEERING'],
                        'platformTypes' => ['ANY_PLATFORM'],
                        'threatEntryTypes' => ['URL'],
                        'threatEntries' => [
                            ['url' => $url]
                        ]
                    ]
                ]
            ]);

            $result = json_decode($response->getBody(), true);
            return !empty($result['matches']);
        } catch (\Exception $e) {
            return false; // In case of an error, consider the URL safe
        }
    }
}
