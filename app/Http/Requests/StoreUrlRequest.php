<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUrlRequest extends FormRequest
{
    public function authorize()
    {
        return true; // Set to false if you want to implement authorization logic
    }

    public function rules()
    {
        return [
            'original_url' => 'required|url|unique:u_r_l_s,original_url',
        ];
    }

    public function messages()
    {
        return [
            'original_url.required' => 'The original URL is required.',
            'original_url.url' => 'The original URL must be a valid URL.',
            'original_url.unique' => 'You already have an url for this.'
        ];
    }
}
