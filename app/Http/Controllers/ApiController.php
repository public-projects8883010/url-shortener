<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrlRequest;
use App\Models\URL as Url;
use App\Services\UrlCheckService;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;

class ApiController extends Controller
{
    protected $urlCheckService;

    public function __construct(UrlCheckService $urlCheckService)
    {
        $this->urlCheckService = $urlCheckService;
    }

    public function store(StoreUrlRequest $request): JsonResponse
    {
        $original_url = $request->input('original_url');

        // Check with Google Safe Browsing API
        if ($this->urlCheckService->isUnsafeUrl($original_url)) {
            return response()->json(['error' => 'The URL is unsafe'], 400);
        }

        // Check if URL already exists
        $url = Url::where('original_url', $original_url)->first();
        if ($url) {
            return response()->json(['short_url' => url($url->short_url)]);
        }

        // Generate a unique short URL
        do {
            $short_url = Str::random(6);
        } while (Url::where('short_url', $short_url)->exists());

        // Save to database
        $url = new Url();
        $url->original_url = $original_url;
        $url->short_url = $short_url;
        $url->save();

        return response()->json(['short_url' => url($short_url)]);
    }
}
