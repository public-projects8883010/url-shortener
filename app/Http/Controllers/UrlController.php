<?php

namespace App\Http\Controllers;

use App\Models\URL as Url;

class UrlController extends Controller
{

    public function show($short_url)
    {
        $url = Url::where('short_url', $short_url)->firstOrFail();
        return redirect($url->original_url);
    }
}
