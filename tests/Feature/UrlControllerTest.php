<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Url;
use App\Services\UrlCheckService;
use Mockery;

class UrlControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_url_shortening()
    {
        $urlCheckService = Mockery::mock(UrlCheckService::class);
        $this->app->instance(UrlCheckService::class, $urlCheckService);
        $urlCheckService->shouldReceive('isUnsafeUrl')->andReturn(false);

        $response = $this->postJson('/api/shorten', [
            'original_url' => 'https://example.com'
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure(['short_url']);
    }

    public function test_url_shortening_with_invalid_url()
    {
        $response = $this->postJson('/api/shorten', [
            'original_url' => 'invalid-url'
        ]);

        $response->assertStatus(422) // Changed from 402 to 422
            ->assertJsonValidationErrors('original_url');
    }

    public function test_duplicate_url_shortening()
    {
        $url = Url::create([
            'original_url' => 'https://example.com',
            'short_url' => 'abc123'
        ]);

        $urlCheckService = Mockery::mock(UrlCheckService::class);
        $this->app->instance(UrlCheckService::class, $urlCheckService);
        $urlCheckService->shouldReceive('isUnsafeUrl')->andReturn(false);

        $response = $this->postJson('/api/shorten', [
            'original_url' => 'https://example.com'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "You already have an url for this.",
                "errors" => [
                    "original_url" => [
                        "You already have an url for this."
                    ]
                ]
            ]);
    }

    public function test_url_redirection()
    {
        $url = Url::create([
            'original_url' => 'https://example.com',
            'short_url' => 'abc123'
        ]);

        $response = $this->get('/abc123');

        $response->assertRedirect($url->original_url);
    }
}
